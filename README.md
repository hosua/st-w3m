# st-w3m

An st build that is compatible with w3m's image displaying capabilities. Unfortunately, the alpha patch does not work with w3m for me so I patched together this build and an alpha build.

To compile st, run ``sudo make clean install`` in the directory that you extracted the files.

## Patches that were applied
```
st-anysize-0.8.4  
st-scrollback-20210507-4536f46        
st-w3m-0.8.3
st-dracula-0.8.2  
st-scrollback-mouse-20191024-a2c479c
```

![image](st-w3m.png)
